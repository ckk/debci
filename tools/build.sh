#!/bin/sh

set -e

exec rerun \
  --name $(basename $0 .sh) \
  --no-notify \
  --background \
  --dir lib,docs \
  --exit \
  -- \
  make
