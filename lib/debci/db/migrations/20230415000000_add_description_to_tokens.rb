class AddDescriptionToTokens < Debci::DB::LEGACY_MIGRATION
  def change
    add_column :keys, :description, :string
  end
end
