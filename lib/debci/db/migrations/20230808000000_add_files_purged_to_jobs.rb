class AddFilesPurgedToJobs < Debci::DB::LEGACY_MIGRATION
  def change
    # We want this flag to be false by default, but there are a lot more jobs
    # that should have it as true than as false. Therefore, we first create the
    # column with a default of true, set it to false in the non-expired jobs,
    # then switch the default to false.
    #
    # This migration strategy causes the migration to run in < 10% of the time
    # compared to if we set the default to false from the beginning, and then
    # update all expired jobs to true.
    add_column :jobs, :files_purged, :boolean, default: true, null: true

    reversible do |direction|
      direction.up do
        deadline = Time.now - Debci.config.data_retention.days
        execute "UPDATE jobs set files_purged = false WHERE date >= '#{deadline}'"
      end
    end

    change_column_default :jobs, :files_purged, from: true, to: false
  end
end
