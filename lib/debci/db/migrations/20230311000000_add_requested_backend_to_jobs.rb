class AddRequestedBackendToJobs < Debci::DB::LEGACY_MIGRATION
  def change
    add_column :jobs, :requested_backend, :string
  end
end
