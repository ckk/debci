require "spec_helper"
require 'spec_mock_server'
require 'debci'
require 'debci/html'
require 'debci/status'
require 'rack/test'
require 'json'
require 'uri'
require 'debci/job'

describe Debci::Status do
  include Rack::Test::Methods

  class Status < Debci::Status
    set :raise_errors, true
    set :show_exceptions, false
  end

  def app
    mock_server('/status', Status)
  end

  context 'handling trailing slashes' do
    it 'redirects /status to /status/' do
      get "/status"
      expect(URI.parse(last_response.location).path).to eq("/status/")
    end
    it 'redirects /status/alerts to /status/alerts/' do
      get "/status/alerts"
      expect(URI.parse(last_response.location).path).to eq("/status/alerts/")
    end
  end

  context 'showing status page' do
    it 'works' do
      Debci::HTML.update # generate graphs
      get "/status/"
      expect(last_response.body).to match(/status/)
      expect(last_response.status).to eq(200)
    end
  end
  let(:theuser) { Debci::User.create!(username: 'debci') }

  context 'showing status alerts page' do
    it 'lists tmpfail jobs' do
      pkg = Debci::Package.create!(name: 'mypackage')
      Debci::Job.create!(package: pkg, suite: 'unstable', arch: arch, status: 'tmpfail', requestor: theuser, date: Time.now)
      get '/status/alerts/'
      expect(last_response.body).to match('mypackage')
    end
  end

  context 'showing status failing page' do
    it 'lists fail jobs' do
      pkg = Debci::Package.create!(name: 'mypackage')
      Debci::Job.create!(package: pkg, suite: 'unstable', arch: arch, status: 'fail', requestor: theuser, date: Time.now)
      get '/status/failing/'
      expect(last_response.body).to match('mypackage')
    end
  end

  context 'showing status pending page' do
    it 'lists pending jobs' do
      pkg = Debci::Package.create!(name: 'mypackage')
      yesterday = Time.now - 1.day
      Debci::Job.create!(package: pkg, suite: 'unstable', arch: arch, status: nil, requestor: theuser, trigger: 'migration', created_at: yesterday)
      get "/status/pending/"
      expect(last_response.body).to match('mypackage')
    end
  end

  context 'showing platform specific issues page' do
    it 'lists platform specific issues jobs' do
      pkg = Debci::Package.create!(name: 'mypackage')
      allow(Debci.config).to receive(:arch_list).and_return(['amd64', 'arm64'])
      Debci::Job.create!(package: pkg, suite: 'unstable', status: 'pass', arch: 'amd64', requestor: theuser, date: Time.now)
      Debci::Job.create!(package: pkg, suite: 'unstable', status: 'fail', arch: 'arm64', requestor: theuser, date: Time.now)
      get '/status/platform-specific-issues/'
      expect(last_response.body).to match('mypackage')
    end
  end

  context 'showing slow page' do
    it 'lists slow jobs' do
      pkg = Debci::Package.create!(name: 'mypackage')
      Debci::Job.create!(package: pkg, suite: 'unstable', arch: arch, status: 'pass', duration_seconds: 4000, requestor: theuser, date: '2019-02-03 11:00')
      get '/status/slow/'
      expect(last_response.body).to match('mypackage')
    end
  end
end
